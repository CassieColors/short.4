#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>

#define MAX_PROCESSES 18
#define ALPHA 3
#define BETA 4
#define CLKKEY 1234
#define PCBKEY 4321
#define READY 10
#define EXIT 20
#define BLOCKED 30
#define SUSPENDED 40
#define RUNNING 50

typedef struct Clock{
	unsigned int seconds;
	unsigned int nanoseconds;
	long msg[2];
} Clock;

typedef struct PCB{
	long PID;
	char status;
	unsigned int readyTime[2];
	unsigned int totalCPUTime;
	unsigned int totalWaitTime;
	unsigned int startTime[2];
	unsigned int finishTime[2];
	unsigned int lastCPUTime;
	unsigned int lastWaitingTime;
	int priority;
} PCB;

// Prototypes
void findTime();
void interruptHandler(int);

// Global variable for handling interrupts
Clock* clockPointer;
PCB* pcbPointer;
int processNumber = 0;
time_t currentTime;
struct tm* timeInfo;
char errorString[100];
sem_t* semaphores;

int main (int argc, char *argv[]) {

	if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
                perror(errorString);
		exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	Clock clock;
	clockPointer = &clock;
	PCB controlBlock;
	pcbPointer = &controlBlock;
	int clkStructID;
	int pcbStructID;
	processNumber = atoi(argv[1]);
	int executing = 1;

	// Retrieve id for shared memory block and attach to it
	clkStructID = shmget(CLKKEY, sizeof(clock), 0666);
	pcbStructID = shmget(PCBKEY, sizeof(controlBlock), 0666);
        if(clkStructID == -1 || pcbStructID == -1){
                sprintf(errorString, "%s: Error: Failed to locate shared memory", argv[0]);
                perror(errorString);
                abort();
        }
        clockPointer = shmat(clkStructID, NULL, 0);
	pcbPointer = shmat(pcbStructID, NULL, 0);
        if(clockPointer == (Clock*)-1 || pcbPointer == (PCB*)-1){
                sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
                perror(errorString);
                abort();
     	}
	
	int previousStatus = pcbPointer[processNumber].status;
	if(pcbPointer[processNumber].status == READY || pcbPointer[processNumber].status == SUSPENDED){
		pcbPointer[processNumber].status = RUNNING;
	} else if(pcbPointer[processNumber].totalCPUTime >= 50000000){
		pcbPointer[processNumber].status = EXIT;
		clockPointer->msg[0] = -1;
		clockPointer->msg[1] = -1;
		executing = 0;
	} else if(pcbPointer[processNumber].status == EXIT){
		clockPointer->msg[0] = -1;
		clockPointer->msg[1] = -1;
		executing = 0;
	}
	
	// Retrieve semaphores
	semaphores = sem_open("semaphore", O_RDWR);
	if(semaphores == SEM_FAILED){
		sprintf(errorString, "%s: Error: Failed to retrieve semaphore", argv[0]);
		perror(errorString);
		abort();
	}	

	int q;

	while(executing){
		if(sem_wait(semaphores) < 0){
			sprintf(errorString, "%s: Error: Failed to wait on semaphore", argv[0]);
			perror(errorString);
			abort();
		}
		//fprintf(stderr, "Child %ld (pcb) %ld(actual) sees dispatched PID %ld\n", pcbPointer[processNumber].PID, (long)getpid(), clockPointer->msg[0]);
		// Check whether or not the process was scheduled
		if(clockPointer->msg[0] == pcbPointer[processNumber].PID){
			pcbPointer[processNumber].totalCPUTime += clockPointer->msg[1];
			pcbPointer[processNumber].lastCPUTime = clockPointer->msg[1];
		
			// Clear message so next process can be scheduled	
			if(previousStatus == SUSPENDED){
				pcbPointer[processNumber].status = SUSPENDED;
			} else if(previousStatus == READY){
				pcbPointer[processNumber].status = READY;
			} else {
				pcbPointer[processNumber].status = EXIT;
				pcbPointer[processNumber].finishTime[0] = clockPointer->seconds;
				pcbPointer[processNumber].finishTime[1] = clockPointer->nanoseconds; 
				executing = 0;
			}
			clockPointer->msg[0] = -1;
                        clockPointer->msg[1] = -1;
		} else {
			pcbPointer[processNumber].totalWaitTime += clockPointer->msg[1];
			pcbPointer[processNumber].status = READY;
		}
		
	
		// After critical section wake next process
		if(sem_post(semaphores) < 0){
			sprintf(errorString, "%s: Error: Failed to signal on semaphore", argv[0]);
			perror(errorString);
			abort();
		}
	}

	// Detach shared memory
	shmdt(clockPointer);
	shmdt(pcbPointer);
	return 0;
}

void findTime(){
	time(&currentTime);
	timeInfo = localtime(&currentTime);
}

// Deliver message based on interrupt and detach memory
void interruptHandler(int SIGNAL){
	if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timeout alert received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	} else if(SIGNAL == SIGABRT){
 		fprintf(stderr, "The program was aborted. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	} else {
		fprintf(stderr, "Interrupt received from parent. Process %d with PID %ld terminating...\n", (processNumber + 1), (long)getpid());
	}
	shmdt(clockPointer);
	shmdt(pcbPointer);
	sem_unlink("semaphore");
	exit(1);
}
