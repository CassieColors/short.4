EXECS = oss user queue
FLAGS = -pthread

all: $(EXECS)

oss: oss.c
	gcc $(FLAGS) -o oss oss.c

user: user.c
	gcc $(FLAGS) -o user user.c

queue: queue.c
	gcc $(FLAGS) -c queue.c
clean:
	rm -f *.out
	rm -f $(EXECS)
	rm -f *.o
