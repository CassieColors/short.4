#ifndef QUEUE
#define QUEUE

typedef struct Node {
	int index;
	struct Node *next;
} Node;

typedef struct Queue {
	Node *head;
	Node *tail;
	int size;
} Queue;

void push(Queue *, int);
int pop(Queue *);
void printQueue(Queue *);
int containsIndex(Queue *, int);
Queue createQueue();

#endif
