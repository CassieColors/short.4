#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h> 
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/shm.h>
#include <time.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdbool.h>
#include "queue.c"

#define MAX_PROCESSES 18
#define ALPHA 3
#define BETA 4
#define CLKKEY 1234
#define PCBKEY 4321
#define READY 10
#define EXIT 20
#define BLOCKED 30
#define SUSPENDED 40
#define RUNNING 50
#define QUANTUM 2000000000

typedef struct Clock{
	unsigned int seconds;
	unsigned int nanoseconds;
	long msg[2];
} Clock;

typedef struct PCB{
	long PID;
	int status;
	unsigned int readyTime[2];
	unsigned int totalCPUTime;
	unsigned int totalWaitTime;
	unsigned int startTime[2];
	unsigned int finishTime[2];
	unsigned int lastCPUTime;
	unsigned int lastWaitingTime;
	int priority;
} PCB;

// Prototypes
void destroySharedMemory();
void interruptHandler(int);
void findTime();
void writeLog(char*);
unsigned int setQuantum(long);
void dispatch(int, int);

// Global variables that need to be accessed in interrupt handler
int PID[MAX_PROCESSES];
Clock* clockPointer;
PCB controlBlock;
PCB* pcbPointer;
int pcbID;
int clkStructID;
int runningProcesses = 0;
time_t currentTime;
struct tm* timeInfo;
sem_t* semaphores;
FILE* logFile;
char msg[256];
unsigned int roundTime = 1000000000;
long long totalWaitTime;
long long totalCPUTime;
int totalProcesses = 0;
int logLines = 0;

int main (int argc, char *argv[]) {
	int hflag = 0;
	int options;

	findTime();
	srand((unsigned)time(0));
	char* fileName;
	char defaultLogs[20];
	sprintf(defaultLogs, "%d.%d-logs%d.txt", (timeInfo->tm_mon + 1), timeInfo->tm_mday, (rand() % 1000));
	fileName = defaultLogs;
	char errorString[100];
	unsigned int timeout = 60;
	Clock clock;
	clockPointer = &clock;
	pcbPointer = &controlBlock;

	// Check for any options passed with the executable and respond accordingly	
	while ((options = getopt (argc, argv, "hl:t:")) != -1){
		switch (options){
			case 'h':
				hflag = 1;
				break;
			case 'l':
				fileName = optarg;
				break;
			case 't':
				timeout = atoi(optarg);
				break;
			case '?':
				// If the flag wasn't recognized, print a list of available options
        			if (isprint (optopt))
					// If the flag passed was a number, the user was probably trying to pass a negative argument
					if(isdigit(optopt)){
						errno = EINVAL;
						sprintf(errorString, "%s: Error: Negative argument", argv[0]);
						perror(errorString);
						fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);

          				} else {
						fprintf (stdout, "Available options\n-- \'h\' (view arguments).\n-- \'l\' (filename)\n-- \'t\' (timer)\n", optopt);
              				}
				return 1;
			default:
				abort();
		}
	}
	// If the hflag is detected, print help message and exit
	if(hflag){
		fprintf(stdout, "%s has three optional arguments.\nUse -l followed by a full filename to specify a filename.\nUse -t followed by a positive integer to specify a timeout limit in seconds.\n", argv[0]);
		return 0;
	}
	if(argc > 5) {
		errno = E2BIG;
		sprintf(errorString, "%s: Error", argv[0]);
		perror(errorString);
		fprintf(stdout, "Enter \'%s -h\' to view a description of arguments for %s\n", argv[0], argv[0]);
		return 1;
	}

	//  Use variables that can be altered by the command line arguments
	alarm(timeout);

	// Set signal handlers
        if(signal(SIGINT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGINT", argv[0]);
		perror(errorString);
	        exit(1);
        }
        if(signal(SIGALRM, interruptHandler) == SIG_ERR){
                sprintf(errorString, "%s: Error catching SIGALRM", argv[0]);
		perror(errorString);
		exit(1);
        }
	if(signal(SIGABRT, interruptHandler) == SIG_ERR){
		sprintf(errorString, "%s: Error catching SIGABRT", argv[0]);
		perror(errorString);
		exit(1);
	}

	// Create and attach shared memory for struct so that it can be accessed by all processes
	clkStructID = shmget(CLKKEY, sizeof(clock), IPC_CREAT | IPC_EXCL | 0666);
	if(clkStructID == -1){
		sprintf(errorString, "%s: Error: Failed to allocate shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}
	clockPointer = shmat(clkStructID, NULL, 0);
	if(clockPointer == (Clock*)-1){
		sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}

      	pcbID = shmget(PCBKEY, (sizeof(controlBlock) * MAX_PROCESSES), IPC_CREAT | 0666);
        if(pcbID == -1){
                sprintf(errorString, "%s: Error: Failed to allocate shared memory", argv[0]);
               	perror(errorString);
                destroySharedMemory();
		return 1;
        }

        pcbPointer = shmat(pcbID, NULL, 0);
	if(pcbPointer == (PCB*)-1){
		sprintf(errorString, "%s: Error: Failed to attach shared memory", argv[0]);
                perror(errorString);
                destroySharedMemory();
        	return 1;
        }

	// Iniitialize semaphores
	semaphores = sem_open("semaphore", O_CREAT, 0666, 1);
	if(semaphores == SEM_FAILED){
		sprintf(errorString, "%s: Error: Failed to create semaphore", argv[0]);
		perror(errorString);
		abort();
	}
        if(sem_close(semaphores) < 0){
                sprintf(errorString, "%s: Error: Failed to close semaphore", argv[0]);
                perror(errorString);
                abort();
        }

	// Create queues
	Queue queues[3];
	int i;
	for (i = 0; i < 3; i++) {
		queues[i] = createQueue();
	}


	// Fill array of child process PIDs and shared Messages array with -1's to initiate them, and initialize clock to zero
	int j, p, l;
	for(j = 0; j < MAX_PROCESSES; j++){
		PID[j] = -1;
		pcbPointer[j].PID = -1;
		pcbPointer[j].status = -1;
		pcbPointer[j].totalCPUTime = -1;
		for(p = 0; p < 2; p++){
			pcbPointer[j].startTime[p] = -1;
			pcbPointer[j].finishTime[p] = -1;
			pcbPointer[j].readyTime[p] = -1;
		}
		pcbPointer[j].lastCPUTime = 0;
		pcbPointer[j].lastWaitingTime = 0;
		pcbPointer[j].totalWaitTime = -1;
        	pcbPointer[j].priority = -1;

	}
	clockPointer->seconds = 0;
	clockPointer->nanoseconds = 0;
	clockPointer->msg[0] = -1;
	clockPointer->msg[1] = -1;
	
	// Open file and check that this was successful
	logFile = fopen(fileName, "a");
	if(logFile == NULL){
		sprintf(errorString, "%s: Error opening log file", argv[0]);
		perror(errorString);
		destroySharedMemory();
		return 1;
	}
	sprintf(msg, "\n\nInitiating oss...\n");
	writeLog(msg);

	int k, m;
	int status;
	int pid;
	int threshold = 3000000000;
	int processIndex = 0;
	long nextForkSeconds = -1;
	long nextForkNanos = -1;
	int roundQuantum = 0;

	// Generate new processes if any of the others have stopped and increment the clock
	while(totalProcesses < 100 && clockPointer->seconds < 60){
		unsigned int timeSeconds = clockPointer->seconds;
		unsigned int timeNanos = clockPointer->nanoseconds;

		if(((nextForkSeconds <= timeSeconds && nextForkNanos <= timeNanos) || (nextForkSeconds < timeSeconds)) && runningProcesses < MAX_PROCESSES){
			unsigned int randomSecs = (rand() % 2);
			unsigned int randomNanos = (rand() % 1000000000);
		
			fprintf(stderr, "Running: %d\n", runningProcesses);
			nextForkSeconds = (randomSecs + timeSeconds);
			nextForkNanos = (randomNanos + timeNanos);
			if(nextForkNanos >= 1000000000){
				nextForkSeconds++;
				nextForkNanos = (nextForkNanos % 1000000000);
			}
			fprintf(stderr, "Next fork: %d:%d\n", nextForkSeconds, nextForkNanos);

			//Build process control block
                        pcbPointer[processIndex].startTime[0] = clockPointer->seconds;
			pcbPointer[processIndex].startTime[1] = clockPointer->nanoseconds;
                        pcbPointer[processIndex].priority = 0;

			if((PID[processIndex] = fork()) == -1){
				sprintf(errorString, "%s: Error: Forking error", argv[0]);
                   		perror(errorString);
                       		abort();
			} else if(PID[processIndex] == 0){
				char pcbIndex[5];
				sprintf(pcbIndex, "%d", processIndex);
	      			char* args[] = {"./user", pcbIndex, NULL};
					
				execvp("./user", args);
	
     	               		// If this block is reached there was an error with exec
       	    			sprintf(errorString, "%s: Exec error within child %d", argv[0], (processIndex + 1));
               			perror(errorString);
                  		abort();
			} else {
				pcbPointer[processIndex].PID = PID[processIndex];
				push(&queues[0], processIndex);
				sprintf(msg, "Process %d with PID %ld started at %d:%d\n", processIndex, PID[processIndex], timeSeconds, timeNanos);
				writeLog(msg);
				fprintf(stderr, "Process %d with PID %ld started at %d:%d\n", processIndex, PID[processIndex], timeSeconds, timeNanos);
				totalProcesses++;
				fprintf(stderr, "Total processes: %d\n", totalProcesses);	
				runningProcesses++;
				processIndex++;
			}
		}

                for(m = 0; m < MAX_PROCESSES; m++){
			if(PID[m] != -1){
                        	if(pcbPointer[m].status == EXIT){
					deleteKey(&queues[0], m);
					totalWaitTime += pcbPointer[m].totalWaitTime;
					totalCPUTime += pcbPointer[m].totalCPUTime;
                                	//Wait for the process to completely finish so that it's PID isn't overwritten before it's done
                                	waitpid(PID[m], &status, 0);
					sprintf(msg, "Process %d with PID %ld finished at %d:%d.\n", m, PID[m], timeSeconds, timeNanos);
                                	writeLog(msg);
					fprintf(stderr, "Process %d with PID %ld finished at %d:%d.\n", m, PID[m], timeSeconds, timeNanos);
                                	PID[m] = -1;
                                	pcbPointer[m].PID = -1;
                                	pcbPointer[m].status = -1;
                                	processIndex = m;
                                	runningProcesses--;
                        	} else if((pcbPointer[m].status == READY || pcbPointer[m].status == SUSPENDED) && (containsIndex(&queues[0], m) == 0)){
                                	if(queues[0].size <= MAX_PROCESSES){
                                        	fprintf(stderr, "Process %d with PID %ld with status %d pushed back onto queue\n", m, PID[m], pcbPointer[m].status);
                                        	push(&queues[0], m);
						//printQueue(&queues[0]);
                                	}
                        	} else if((pcbPointer[m].status == BLOCKED) && (containsIndex(&queues[0], m) == 0)){
                                	if(queues[0].size <= MAX_PROCESSES && ((pcbPointer[m].readyTime[0] <= clockPointer->seconds) && pcbPointer[m].readyTime[1] <= clockPointer->nanoseconds) || (pcbPointer[m].readyTime[0] < clockPointer->seconds)){
                                        	pcbPointer[m].status = READY;
						push(&queues[0], m);
                                	}
                        	}
			}
                }


		// If the scheduled process finished its turn and cleared the message, schedule another one
		if(clockPointer->msg[0] == -1 && clockPointer->msg[1] == -1 && runningProcesses > 0){	
			// Dispatch a process
			int tempIndex;
			unsigned int roundQuantum = 0;
			if(queues[0].size != 0){
				tempIndex = pop(&queues[0]);
				roundQuantum = setQuantum(tempIndex);
				if(PID[tempIndex] != -1){
					dispatch(tempIndex, roundQuantum);
					sprintf(msg, "Process %d with PID %ld with status %d dispatched!\n", tempIndex, PID[tempIndex], pcbPointer[tempIndex].status);
					writeLog(msg);
					fprintf(stderr, "Process %d with PID %ld with status %d dispatched!\n", tempIndex, PID[tempIndex], pcbPointer[tempIndex].status);
					sprintf(msg, "Set quantum for process %ld at %d.\n", PID[tempIndex], roundQuantum);
					writeLog(msg);
				}
			} else if(queues[1].size != 0){
				tempIndex = pop(&queues[0]);
				roundQuantum = setQuantum(tempIndex);
				dispatch(tempIndex, roundQuantum);
			} else if(queues[2].size != 0){
				tempIndex = pop(&queues[0]);
				roundQuantum = setQuantum(tempIndex);
				dispatch(tempIndex, roundQuantum);
			} else {
				fprintf(stderr, "Attempted to dispatch but found an empty queue!\n");
			}
			roundTime = roundQuantum;			
		} else {
			roundTime = 0;
		}

		// Increment oss clock
		clockPointer->nanoseconds += (roundTime + (rand() % 1001));
		if(clockPointer->nanoseconds >= 1000000000){
			clockPointer->nanoseconds = (clockPointer->nanoseconds % 1000000000);
			clockPointer->seconds++;
		}
	}

	if(clockPointer->seconds >= 60){
		fprintf(stdout, "60 seconds elapsed in %s. Shutting down...\n",  argv[0]);
		sprintf(msg, "2 seconds elapsed in %s.\n", argv[0]);
		writeLog(msg);
		alarm(1);
	}
	if(totalProcesses >= 100){
		fprintf(stdout, "%s generated 100 processes. Shutting down...\n", argv[0]);
       		sprintf(msg, "%s generated 100 processes.\n", argv[0]);
        	writeLog(msg);
		abort();
	}

	// Wait for all child processes to end and clear them from the PID array as they do
	while(runningProcesses > 0){
		pid = wait(&status);
		for(m = 0; m < MAX_PROCESSES; m++){
			if(PID[m] == pid){
				PID[m] = -1;	
			}
		}
		runningProcesses--;
	}

	// Detach and remove shared memory, close log file, and deallocate dynamic PID array
	destroySharedMemory();
	fclose(logFile);
	return 0;
}

void interruptHandler(int SIGNAL){
	int i = 0;
	char msg[100];
	// Check which signal was receieved and print appropriate message
	if(SIGNAL == SIGINT){
		fprintf(stderr, "Ctrl-C interrupt detected. Terminating all processes and deallocating shared memory at %d:%d...\n", clockPointer->seconds, clockPointer->nanoseconds);
	} else if(SIGNAL == SIGALRM){
		fprintf(stderr, "Timer expired. Terminating all processes and deallocating shared memory...\n");
	}
	if(SIGNAL == SIGALRM || SIGNAL == SIGABRT){
		// Kill all child processes
		for(i = 0; i < MAX_PROCESSES; i++){
			if(PID[i] != -1){
				if (kill(PID[i], SIGNAL) == 0){
					runningProcesses--;
				}	
			}
		}
	}
	// Sleep briefly so that memory destruction message prints last
	sleep(1);

	fprintf(stderr, "REPORT:\nTotal Wait Time: %ld\nTotal CPU Time: %ld\nAverage Wait Time: %ld\nAverage CPU Time: %ld\n", totalWaitTime, totalCPUTime, (totalWaitTime/totalProcesses), (totalCPUTime/totalProcesses));
	sprintf(msg, "REPORT:\nTotal Wait Time: %ld\nTotal CPU Time: %ld\nAverage Wait Time: %ld\nAverage CPU Time: %ld\n", totalWaitTime, totalCPUTime, (totalWaitTime/totalProcesses), (totalCPUTime/totalProcesses)); 
	writeLog(msg);
	// Detach and remove shared memory
	destroySharedMemory();
	exit(1);
}

void schedule(){
	
}

unsigned int setQuantum(long processIndex){
    unsigned int quantumChoice = (rand() % 4);
    int r, s;
    unsigned int quantum;
    float percentOfQuantum;
    fprintf(stderr, "Setting quantum...\n");
    switch(quantumChoice){
        case 0:
        	pcbPointer[processIndex].status = EXIT;
		quantum = 0;
    	    	break;
   	case 1:
    	    	if(pcbPointer[processIndex].priority == 0) quantum = QUANTUM;
    		if(pcbPointer[processIndex].priority == 1) quantum = (QUANTUM/2);
    		if(pcbPointer[processIndex].priority == 2) quantum = (QUANTUM/4);
    		pcbPointer[processIndex].status = READY;
    		break;
    	case 2:
    		r = (rand() % 6);
    	    	s = (rand() % 1000);
    		pcbPointer[processIndex].readyTime[0] = clockPointer->seconds + r;
		pcbPointer[processIndex].readyTime[1] = clockPointer->nanoseconds + s;
    		pcbPointer[processIndex].status = BLOCKED;
		if(pcbPointer[processIndex].priority == 0) quantum = QUANTUM;
                if(pcbPointer[processIndex].priority == 1) quantum = (QUANTUM/2);
                if(pcbPointer[processIndex].priority == 2) quantum = (QUANTUM/4);
    		break;
        case 3:
    		percentOfQuantum = ((float)(rand() % 100) / 100);
        	if(pcbPointer[processIndex].priority == 0) quantum = QUANTUM;
        	if(pcbPointer[processIndex].priority == 1) quantum = (QUANTUM/2);
        	if(pcbPointer[processIndex].priority == 2) quantum = (QUANTUM/4);
        	quantum = (quantum * percentOfQuantum);
        	pcbPointer[processIndex].status = SUSPENDED;
        	break;
    }
	roundTime = quantum;
	return quantum;
}

void dispatch(int processIndex, int quantum){
    fprintf(stderr, "Dispatching process %d with PID %ld...\n", processIndex, PID[processIndex]);
    clockPointer->msg[0] = PID[processIndex];
    clockPointer->msg[1] = quantum;
}

void destroySharedMemory(){
	fprintf(stdout, "Destroying shared memory...\n");
	shmdt(clockPointer);
	shmctl(clkStructID, IPC_RMID, NULL);
	shmdt(pcbPointer);
	shmctl(pcbID, IPC_RMID, NULL);

	sem_unlink("semaphore");
}

void findTime(){
        time(&currentTime);
        timeInfo = localtime(&currentTime);
}

void writeLog(char* msg){
	char errorString[100];
	if(logLines <= 1000){
		if(fputs(msg, logFile) < 0){
			sprintf(errorString, "oss: Error writing to log file\n");
			perror(errorString);
			abort();	
		}
		logLines++;
	}
}
