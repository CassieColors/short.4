SYNOPSIS 
	This program simulates an operating system that schedules processes and constructs their process control blocks in shared
	memory. It uses three queues to separate processes by priority, where queue0 is highest priority and queue2 is lowest priority.
	A simulated clock controlled by the master process in this program controls the speed of process generation and timed operations
	within processes.

	** I was able to schedule processes consistently with one queue while not generating new processes (after I fixed a tricky error with overflow in
	my time calculations) but when I tried to generate new processes after others finished I started having issues with dispatching. I've made sure 
	to clear a process from the queue when it finishes but dispatching still stops after a variable number of processes, almost always after a 
	process is dispatched with an EXIT status, but sometimes arbitrarily. Unfortunately I ran out of time to continue debugging.
	
	** Features that I didn't implement:
		multiple queues, including the logic to shift processes between queues
		complete time report
		user process generating a random binary to decide if it uses it's whole quantum

EXAMPLE CALL

	'./oss' calls the main executable with default values for file name and timeout length. These two values can also be
	passed via command line using the flags '-f' and '-t' respectively. For file name, if the desired file is not in the same
	directory as this executable, a path relative to the executable must be used.

	For example:
		'./oss -t 3' will make the program time out after 3 seconds

HELP
	For a list of the arguments that can be passed to each of these programs, use command './oss -h'. These arguments must
	be passed to the program with their respective flags (listed above and via -h) or they will be ignored.

	For example: './oss -h'

SETUP

	All necessary files are compiled by a single Makefile, which builds executables (named without extensions).
	Bitbucket is used for version control, and has been attached using an SSH key.
	Use command 'git log' to view a history of all commits and pushes from the project's home directory
	'o2-short.4' in MY home directory. I've also included a file that contains a copy of my final git log, for viewing
	outside of my home directory.

	GIT URL: https://bitbucket.org/CassieColors/short.4/src

CLEANUP

	Executing the command 'make clean' that is included with the Makefile will remove all executables.
