#include "queue.h"
#include <stdlib.h>
#include <stdio.h>

void push(Queue *queue, int index) {
	Node *newNode;

	if ((newNode = calloc(1, sizeof(Node))) == NULL) {
		perror("Error: Failed to allocate memory for new node in queue");
		return;
	}

	newNode->next = NULL;
	newNode->index = index;

	if (queue->head == NULL) {
		queue->head = newNode;
	}
	else {
		queue->tail->next = newNode;
	}

	queue->tail = newNode;

	queue->size++;
}

int pop(Queue *queue) {
	Node *node = queue->head;
	int index = node->index;

	queue->head = node->next;

	free(node);

	queue->size--;

	return index;
}

void printQueue(Queue *queue) {
	Node *node = queue->head;

	printf("Queue:\n");

	if (node == NULL) {
		printf("Queue is empty\n");
	}

	while (node != NULL) {
		printf("%d\n", node->index);
		node = node->next;
	}
}

int containsIndex(Queue *queue, int index){
	Node *node = queue->head;
	int contains = 0;
	if(node == NULL){
		return contains;
	}
	while(node != NULL){
		if(node->index == index){
			contains = 1;
			break;
		}
		if(node->next != NULL){
			 node = node->next;
		} else {
			break;
		}
	}
	return contains;
}

void deleteKey(Queue *queue, int key){
	Node *temp = queue->head;
	Node *prev;
 
	while (temp != NULL && temp->index == key){
        	queue->head = temp->next;
		free(temp);
		temp = queue->head;
	}
	while (temp != NULL){
       		while (temp != NULL && temp->index != key){
			prev = temp;
			temp = temp->next;
		}
 		if (temp == NULL) return;
 		prev->next = temp->next;
 		free(temp); 
		temp = prev->next;
	}
}


Queue createQueue() {
	Queue queue = {NULL, NULL, 0};

	return queue;
}
